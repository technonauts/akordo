module gitlab.com/technonauts/akordo

go 1.14

require (
	github.com/bwmarrin/discordgo v0.20.3
	gitlab.com/technonauts/akordo-plugins v0.0.0-20200514215712-710481b7b9a0
)
