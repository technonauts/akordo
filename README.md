# Akordo
A Discord chat bot written by the members of the Technonauts chat group.

[![pipeline status](https://gitlab.com/technonauts/akordo/badges/master/pipeline.svg)](https://gitlab.com/technonauts/akordo/-/commits/master)

[Join the Technonauts!](https://discord.gg/A2uuCUr)

## Using the Bot:
1. [Setup your Discord application](https://discordapp.com/developers/applications/)
3. Invite your bot to your server
4. Download the source code `git pull git.sr.ht/~jrswab/akordo`
5. Build the binary: `go build`
6. Run the binary: `./akordo -t <your bot token here>`
  - You may omit `-t` if using the `BOT_TOKEN` environment variable

### Currently required environment variables:
- `BOT_OWNER`
- `BOT_ID`
- `GIPHY_KEY` (only if using the `gif` command)

### Running Akordo as a Service with Systemd
1. Make sure the binary and data  directory is in `/user/local/bin/akordo`
3. Create the service shown below in `/etc/systemd/system` named `akordo.service`
3. Enable startup on boot `systemctl enable akordo.service`
4. Start the bot: `systemctl start akordo.service`
```
[Unit]
Description=Akordo Service
ConditionPathExists=/usr/local/bin/akordo/akordo
After=network.target

[Service]
Type=simple

Environment="BOT_TOKEN=xxxxx"
Environment="BOT_ID=xxxxx"
Environment="BOT_OWNER=xxxxx"
Environment="GIPHY_KEY=xxxxx"

WorkingDirectory=/usr/local/bin/akordo/
ExecStart=/usr/local/bin/akordo/akordo

Restart=on-failure
RestartSec=5s

StandardOutput=syslog
```

Gopher logo by Kurio Ari; inspired by the Rene French original.
